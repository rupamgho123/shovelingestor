

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;

public class Main {

	public static void main(String[] args) throws Exception {
		PropertyConfigurator.configure("config/log4j.properties");

		String entityName = "invoice";
		HashMap<String, Object> criterion = new HashMap<String, Object>();
		criterion.put("bu_id", "fkmp");
		criterion.put("type", "PayableCreditNote");
		criterion.put("created_at_from", "2014-08-01");
		criterion.put("created_at_to", "2014-08-26");
		List<String> exclusionValues = new LinkedList();
		exclusionValues.add("created");
		exclusionValues.add("voided");
		exclusionValues.add("canceled");
		criterion.put("status", exclusionValues);
		ExtractionThread thread = new ExtractionThread(entityName, criterion, 0);
		thread.start();
		
	}

}
