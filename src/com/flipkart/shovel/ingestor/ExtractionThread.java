

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class ExtractionThread extends Thread {

	Logger logger = Logger.getLogger(ExtractionThread.class);
	int pageNumber;
	EntityExtractor extractor;

	ExtractionThread(String entityName, HashMap<String, Object> criterion, int pageNumber) {
		extractor = new EntityExtractor(entityName, criterion);
		this.pageNumber = pageNumber;
	}

	@Override
	public void run() {
		super.run();
		while (true) {
			try {
				enqueueForEventPublishing();
			} catch (Exception ex) {
				logger.info(ExceptionUtils.getStackTrace(ex));
				ex.printStackTrace();
			}			
		}
	}

	private String convertHashToString(HashMap<String, Object> criterion)
			throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(criterion);
	}

	private void enqueueForEventPublishing() throws JsonGenerationException,
			JsonMappingException, IOException {
		logger.info("Enqueuing the details for event publishing");
		try {
			
			List<HashMap<String, Object>> list = extractor
					.extract(pageNumber);
			if(list == null || list.size() == 0) {
				logger.info("No more rows found to publishe the events for the criterion");				
				try {
					Thread.sleep(180000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("Shutting down the system");
				System.exit(0);
			}
			
			HashMap<String, Object> first = list.get(0);
			HashMap<String, Object> last = list.get(list.size() - 1);						
			Long stagedDetailsId = ExtractorDBWrapper.saveExtractedEntityDetails(pageNumber,
					(Timestamp) last.get("created_at"),
					(Timestamp) first.get("created_at"), list.size(),
					extractor.getEntityName(),
					convertHashToString(extractor.getCriterion()));			

			try {
				logger.info("Enqueing the entityDetails for Id: " + stagedDetailsId);				
				for (HashMap<String, Object> item : list) {
					EventProcessor.getEventProcessor().threadPoolExecutor
							.submit(new EventPublishThread(item, stagedDetailsId,
									extractor.getEntityName()));
				}
				
				logger.info("Enqueued #" + list.size() + " events for reIngestion Id : " + stagedDetailsId);
			} catch (Exception ex) {
				logger.info("Enqueuing all the events for ID : " + stagedDetailsId + " failed");
				logger.info(ExceptionUtils.getStackTrace(ex));
			}

			pageNumber++;
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info(ExceptionUtils.getStackTrace(e));
		}
	}
}