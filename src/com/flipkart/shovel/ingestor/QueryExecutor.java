

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.dataformat.yaml.snakeyaml.Yaml;

public class QueryExecutor {

	String entityName;
	String buId;
	private final String configFileLocation = "/Volumes/WORK/BF/shovelingestor/config/entity_source_credentials.yml";
	private ExtractorDBWrapper sqlite;
	private SourceConnectionManager mysql;
	private Map<String, Object> dbConfig;
	private Logger logger = Logger.getLogger(QueryExecutor.class);

	public QueryExecutor(String entityName, String buId) {
		this.entityName = entityName;
		this.buId = buId;

		InputStream input = null;
		try {
			input = new FileInputStream(new File(configFileLocation));
		} catch (FileNotFoundException e) {
			logger.error(configFileLocation + " not found", e);
			throw new ExceptionInInitializerError(e);
		}

		String key = entityName;
		if (buId != null) {
			key = buId + "_" + entityName;
		}

		Yaml yaml = new Yaml();
		dbConfig = (Map<String, Object>) yaml.load(input);
		dbConfig = (Map<String, Object>) dbConfig.get(key);

		try {
			connect();
		} catch (ClassNotFoundException e) {
			logger.info(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
		} catch (SQLException e) {
			logger.info(ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
		}
	}

	private void connect() throws ClassNotFoundException, SQLException {
		sqlite = new ExtractorDBWrapper();
		sqlite.connect();

		mysql = new SourceConnectionManager((String) dbConfig.get("host_name"),
				(String) dbConfig.get("db_name"),
				(String) dbConfig.get("user"),
				(String) dbConfig.get("password"));
		mysql.connect();
	}

	public ResultSet execute(String sql) throws SQLException {
		Statement stmt = SourceConnectionManager.mysqlConnection.createStatement();
		return stmt.executeQuery(sql);
	}
}
