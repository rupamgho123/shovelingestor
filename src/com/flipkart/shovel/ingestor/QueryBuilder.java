
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.dataformat.yaml.snakeyaml.Yaml;

public class QueryBuilder {
	private final Logger logger = Logger.getLogger(QueryBuilder.class);
	String entityName;
	Map<String, Object> configMap;
	private final String configFileLocation = "/Volumes/WORK/BF/shovelingestor/config/entity_extractor.yml";

	@SuppressWarnings("unchecked")
	public QueryBuilder(String entityName) {
		this.entityName = entityName;
		InputStream input = null;
		try {
			input = new FileInputStream(new File(configFileLocation));
		} catch (FileNotFoundException e) {
			logger.error(configFileLocation + " not found", e);
			throw new ExceptionInInitializerError(e);
		}

		Yaml yaml = new Yaml();
		configMap = (Map<String, Object>) yaml.load(input);
		configMap = (Map<String, Object>) configMap.get(entityName);

		try {
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getQuery(Map<String, Object> criterion, int pageNo) {
		StringBuilder query = new StringBuilder();
		query = query.append("select ");
		if (configMap.get("select") != null) {
			query = query.append(configMap.get("select"));
		} else {
			query.append("*");
		}
		query.append(" from ");
		if (configMap.get("table") != null) {
			query.append(configMap.get("table"));
		} else {
			// throws exception
		}
		query.append(" where ");
		String[] placeHolders = configMap.get("condition").toString()
				.split(",");
		int count = 0;
		for (String placeHolder : placeHolders) {
			count++;
			if (criterion.containsKey(placeHolder))
				query.append(" " + placeHolder + " = '"
						+ criterion.get(placeHolder) + "'");
				
			if(count < placeHolders.length) {
				query.append(" AND ");			
			}
		}
		
		String exclusionColumn = configMap.get("exclusion").toString();
		if(criterion.containsKey(exclusionColumn)) {
			List<String> exclusionValues = (List<String>) criterion.get(exclusionColumn);
			String inCondition = "(";
			int valueCount = 0;
			for(String value : exclusionValues) {
				valueCount++;
				inCondition += "'" + value + "'";
				if(valueCount < exclusionValues.size()) {
					inCondition += ", ";
				}
			}
			inCondition += ")";
			query.append(" AND " + exclusionColumn + " NOT IN " + inCondition);
		}
		String rangeColumn = configMap.get("range").toString();
		String startRangeColumn = rangeColumn + "_from";
		String endRangeColumn = rangeColumn + "_to";
		if(criterion.containsKey(startRangeColumn) && criterion.containsKey(endRangeColumn)) {
			query.append(" AND " + rangeColumn + " >= '" + criterion.get(startRangeColumn) +
					"' AND " + rangeColumn + " <= '" + criterion.get(endRangeColumn) + "'");
		}
		
		int batchSize = 1000;
		if (configMap.containsKey("batch_size")) {
			batchSize = Integer.valueOf(configMap.get("batch_size").toString());
		}
		int offset = 0;
		if (pageNo != 0) {
			offset = (pageNo * batchSize) + 1;
		}
		String limit = " LIMIT " + offset + ", " + batchSize;
		query.append(limit);
		return query.toString();
	}

	public static void main(String[] args) {
		QueryBuilder builder = new QueryBuilder("invoice");
		Map<String, Object> criterion = new HashMap<String, Object>();
		criterion.put("type", "RevenueInvoice");
		criterion.put("bu_id", "fkmp");
		criterion.put("created_at_from", "2013-05-01");
		criterion.put("created_at_to", "2013-05-02");
		List<String> exclusionValues = new LinkedList();
		exclusionValues.add("created");
		exclusionValues.add("voided");
		exclusionValues.add("canceled");
		criterion.put("status", exclusionValues);
		System.out.println("Here is the config : "
				+ builder.getQuery(criterion, 0));

	}
}
