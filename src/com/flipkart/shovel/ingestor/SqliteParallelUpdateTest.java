

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class SqliteParallelUpdateTest {

	static final Logger logger = Logger
			.getLogger(SqliteParallelUpdateTest.class);

	public static void increment(Connection sqliteConnection) {
		try {
			Statement stmt = sqliteConnection.createStatement();
			String sql = "update INCREMENT set COUNTER = COUNTER + 1;";
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException e) {
			System.out.println("Unable to update the increment count, Error : "
					+ e.getMessage());
			e.printStackTrace();
			logger.info(ExceptionUtils.getStackTrace(e));
		}
	}

	public static int getCount(Connection sqliteConnection) throws SQLException {
		Statement stmt = null;
		try {
			stmt = sqliteConnection.createStatement();
			String sql = "select COUNTER from INCREMENT;";
			ResultSet result = stmt.executeQuery(sql);
			return result.getInt("COUNTER");
		} catch (SQLException e) {
			System.out.println("Unable to update the increment count, Error : "
					+ e.getMessage());
			e.printStackTrace();
			logger.info(ExceptionUtils.getStackTrace(e));
		} finally {
			stmt.close();
		}
		return 0;
	}

	public static void main(String[] args) throws SQLException,
			InterruptedException, ClassNotFoundException {

		Class.forName("org.sqlite.JDBC");
		final Connection[] connections = new Connection[4];
		for (int i = 0; i < 4; i++) {
			connections[i] = DriverManager
					.getConnection("jdbc:sqlite:db_file.db");
		}

		// createTables(sqliteConnection);

		Thread[] threads = new Thread[100];
		for (int i = 0; i < 100; i++) {
			final int id = i % 4;
			threads[i] = new Thread() {
				public void run() {
					increment(connections[0]);
				}
			};
			threads[i].start();
			System.out.println("Started the thread " + i);
			System.out.println("Count : " + getCount(connections[0]));
		}

		for (int i = 0; i < 100; i++) {
			threads[i].join();
		}

	}

	public static void createTables(Connection sqliteConnection) {
		try {
			Statement stmt = sqliteConnection.createStatement();
			String sql = "CREATE TABLE INCREMENT(COUNTER INT);";
			stmt.executeUpdate(sql);
			stmt.close();

			logger.info("table initialized");
		} catch (SQLException ex) {
			logger.info("table creation failed");
			logger.info(ExceptionUtils.getStackTrace(ex));
		}
	}
}
