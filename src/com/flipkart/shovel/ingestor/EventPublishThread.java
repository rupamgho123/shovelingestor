

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.FluentCaseInsensitiveStringsMap;
import com.ning.http.client.Response;

public class EventPublishThread implements Runnable {

	private String URL = "http://shovel.stage.ch.flipkart.com/events";
	Logger logger = Logger.getLogger(EventPublishThread.class);
	HashMap<String, Object> item;
	Long stagedDetailsId;
	private String entityName;
	AsyncHttpClient asyncHttpClient;	

	public EventPublishThread(HashMap<String, Object> item, Long stagedDetailsId, String entityName) {
		this.item = item;
		this.stagedDetailsId = stagedDetailsId;
		this.entityName = entityName;
		asyncHttpClient = new AsyncHttpClient();		
	}

	public void run() {
		try {
			
			String eventPayload = EventConstructor.getInstance(entityName).getEventPayload(item);
			int statusCode = makePostCall(eventPayload);			
			asyncHttpClient.close();
			ExtractorDBWrapper.updateProcessedCount(stagedDetailsId);
		} catch (Exception ex) {
			logger.info(ExceptionUtils.getStackTrace(ex));
		}
	}

	 public Response executePost(String url, String body, Map<String, String> headers)
	            throws InterruptedException, ExecutionException, IOException {

	        final FluentCaseInsensitiveStringsMap map = new FluentCaseInsensitiveStringsMap();
	        for(Map.Entry<String, String> entry: headers.entrySet()) {
	            map.add(entry.getKey(), entry.getValue());
	        }

	        return asyncHttpClient.preparePost(url).setBody(body).setHeaders(map).execute().get();
	    }
	 
	public int makePostCall(String body) {
		Response response;
		try {
			Map headerMap = new HashMap();
			headerMap.put("Content-Type", "application/json");
			response = executePost(URL, body, headerMap);
			if(response.getStatusCode() == 201) {
				logger.info("Successfully raised the event for body: " + body);
			}else {
				logger.info("Unable to raise event, here is the error: " + response.getStatusCode());
			}
			return response.getStatusCode();	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 500;
	}

}
