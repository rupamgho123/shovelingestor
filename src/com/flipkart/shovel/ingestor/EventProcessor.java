

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class EventProcessor {

	private Logger logger = Logger.getLogger(EventProcessor.class);
	private static EventProcessor instance;
	int CORE_POOL_SIZE = 50;
	int MAX_POOL_SIZE = 80;
	int CAPACITY = 5000000;
	long KEEP_ALIVE_TIME = 1000;
	public ThreadPoolExecutor threadPoolExecutor;

	EventProcessor() {
		threadPoolExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE,
				MAX_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS,
				new LinkedBlockingQueue<Runnable>(CAPACITY));
		threadPoolExecutor
				.setRejectedExecutionHandler(new RejectedExecutionHandler() {

					public void rejectedExecution(Runnable r,
							ThreadPoolExecutor executor) {
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							logger.info(ExceptionUtils.getStackTrace(e));
						}
						executor.submit(r);
						executor.prestartAllCoreThreads();
					}
				});
	}

	public static EventProcessor getEventProcessor() {
		if (instance == null)
			instance = new EventProcessor();
		return instance;
	}
}
