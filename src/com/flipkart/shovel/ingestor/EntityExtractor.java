

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EntityExtractor {

	private String entityName;
	private HashMap<String, Object> criterion;
	private QueryBuilder queryBuilder;
	private QueryExecutor queryExecutor;

	public EntityExtractor(String entityName, HashMap<String, Object> criterion) {
		this.setEntityName(entityName);
		this.criterion = criterion;
		queryBuilder = new QueryBuilder(entityName);
		queryExecutor = new QueryExecutor(entityName,
				(String) criterion.get("bu_id"));
	}

	public List<HashMap<String, Object>> extract(int pageNo)
			throws SQLException {
		String query = queryBuilder.getQuery(criterion, pageNo);
		ResultSet result = queryExecutor.execute(query);
		return resultSetToArrayList(result);
	}

	private List<HashMap<String, Object>> resultSetToArrayList(ResultSet rs)
			throws SQLException {
		ResultSetMetaData md = rs.getMetaData();
		int columns = md.getColumnCount();
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		while (rs.next()) {
			HashMap<String, Object> row = new HashMap<String, Object>(columns);
			for (int i = 1; i <= columns; ++i) {
				row.put(md.getColumnName(i), rs.getObject(i));
			}
			list.add(row);
		}
		return list;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public HashMap<String, Object> getCriterion() {
		return criterion;
	}
}
