
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class ExtractorDBWrapper {

	static final Logger logger = Logger.getLogger(ExtractorDBWrapper.class);
	private static final String TABLE_NAME = "EXTRACTED_ENTITY_DETAILS";
	private static Connection extractorDBConnection;

	public void connect() throws ClassNotFoundException, SQLException {
		extractorDBConnection = DriverManager.getConnection("jdbc:mysql://"
				+ "localhost" + "/" + "reingestion", "root", "");

		//createTables();
	}

	private void createTables() {
		try {
			Statement stmt = extractorDBConnection.createStatement();
			String sql = "CREATE TABLE " + TABLE_NAME
					+ "(ID 				INTEGER 	NOT NULL PRIMARY KEY AUTO_INCREMENT,"
					+ " PAGE_NUMBER	    INTEGER     NOT NULL, "
					+ " FROM_DATE	    TEXT        NOT NULL, "
					+ " TO_DATE	        TEXT        NOT NULL, "
					+ " ENTITY_NAME     TEXT        NOT NULL, "
					+ " CRITERION       TEXT        NOT NULL, "
					+ " EXTRACT_COUNT   INTEGER     NOT NULL, "
					+ " PUBLISHED_COUNT INTEGER     NOT NULL, "
					+ " CREATED_AT      TEXT        NOT NULL" + " )";
			stmt.executeUpdate(sql);
			stmt.close();

			logger.info("table initialized");
		} catch (SQLException ex) {
			logger.info("table creation failed");
			logger.info(ExceptionUtils.getStackTrace(ex));
		}
	}

	public static Long saveExtractedEntityDetails(int pageNumber, Timestamp fromDate,
			Timestamp endDate, int totalCount, String entityName, String criterion) {
		Long stageDetailsId = -1L;
		try {
			String insertQuery = "INSERT INTO "
					+ TABLE_NAME
					+ " (ID,PAGE_NUMBER,FROM_DATE,TO_DATE,ENTITY_NAME,CRITERION,EXTRACT_COUNT,PUBLISHED_COUNT,CREATED_AT) "
					+ "VALUES (NULL,?,?,?,?,?,?,?,?);";
			PreparedStatement stmt = extractorDBConnection
					.prepareStatement(insertQuery, PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, pageNumber);
			stmt.setTimestamp(2, fromDate);
			stmt.setTimestamp(3, endDate);
			stmt.setString(4, entityName);
			stmt.setString(5, criterion);
			stmt.setInt(6, totalCount);
			stmt.setInt(7, 0);
			stmt.setDate(8, new java.sql.Date(System.currentTimeMillis()));

			Boolean done = stmt.execute();		
			logger.info("data insertion success");
			ResultSet resultset = stmt.getGeneratedKeys();			
			if(resultset.next()) {
				stageDetailsId = resultset.getLong(1);
			}
			stmt.close();
						
		} catch (SQLException ex) {
			logger.info("data insertion failed");
			logger.info(ExceptionUtils.getStackTrace(ex));
		}
		return stageDetailsId;
	}

	public static boolean ifPageExists(String jobId) throws SQLException {
		PreparedStatement prepareStatement = extractorDBConnection
				.prepareStatement("SELECT * FROM " + TABLE_NAME
						+ " WHERE PAGE_NUMBER = ?");
		prepareStatement.setString(1, jobId);
		ResultSet result = prepareStatement.executeQuery();
		boolean next = result.next();

		result.close();
		prepareStatement.close();

		return next;
	}

	public static void updateProcessedCount(Long stagedDetailsId) {
		synchronized (stagedDetailsId) {
			PreparedStatement prepareStatement;
			try {
				prepareStatement = extractorDBConnection
						.prepareStatement("UPDATE "
								+ TABLE_NAME
								+ " SET PUBLISHED_COUNT = PUBLISHED_COUNT + 1 WHERE ID = ?");
				prepareStatement.setLong(1, stagedDetailsId);
				prepareStatement.executeUpdate();
				prepareStatement.close();
			} catch (SQLException e) {
				logger.error("Unable to update the published count  for ID " + stagedDetailsId + ", ERROR: " + e.getMessage());
				e.printStackTrace();
			}
			
		}
		
	}
}
