
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.fasterxml.jackson.dataformat.yaml.snakeyaml.Yaml;


public class EventConstructor {
	
	private final Logger logger = Logger.getLogger(EventConstructor.class);
	Map<String, Object> eventConstructionDetails;
	private final String configFileLocation = "/Volumes/WORK/BF/shovelingestor/config/event_constructor.yml";
	String entityName;
	private static EventConstructor INSTANCE;
	
	public static EventConstructor getInstance(String entityName) {
		if(INSTANCE == null) {
			INSTANCE = new EventConstructor(entityName);
		}
		return INSTANCE;
	}
	
	public EventConstructor(String entityName) {		
		InputStream input = null;
		try {
			input = new FileInputStream(new File(configFileLocation));
		} catch (FileNotFoundException e) {
			logger.error(configFileLocation + " not found", e);
			throw new ExceptionInInitializerError(e);
		}
		this.entityName = entityName;
		Yaml yaml = new Yaml();
		eventConstructionDetails = (Map<String, Object>) yaml.load(input);
		eventConstructionDetails = (Map<String, Object>) eventConstructionDetails.get(entityName);
	}

	public String getHeader(Map entityDetails) throws JsonGenerationException,
			JsonMappingException, IOException {
		String headerKey = eventConstructionDetails.get("header").toString();
		if (headerKey == null) {
			return null;
		}

		Map headerMap = new HashMap();
		headerMap.put("X_BU_ID", entityDetails.get(headerKey));
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(headerMap);
	}

	public String getEventContextJson(Map entityDetails)
			throws JsonGenerationException, IOException {
		Map contextMap = new HashMap();
		String[] contextParams = eventConstructionDetails.get("context")
				.toString().replace(" ", "").split(",");
		for (String param : contextParams) {
			contextMap.put(param, entityDetails.get(param));
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(contextMap);
	}

	public String getGroupId(Map entityDetails) {
		String groupIdKey = eventConstructionDetails.get("group_id").toString();
		return entityDetails.get(groupIdKey).toString();
	}

	public String getEventPayload(Map entityDetails)
			throws JsonGenerationException, JsonMappingException, IOException {		
		Map eventPayload = new HashMap();
		eventPayload.put("entity_name", entityName);
		eventPayload.put("headers", getHeader(entityDetails));
		eventPayload.put("context", getEventContextJson(entityDetails));
		eventPayload.put("group_id", getGroupId(entityDetails));
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(eventPayload);
	}

	public static void main(String[] args) throws JsonGenerationException,
			JsonMappingException, IOException {
		Map details = new HashMap();
		details.put("bu_id", "fkmp");
		details.put("client_ref_id", "JHGJ78687");
		details.put("type", "RevenueInvoice");
		details.put("id", 182739098);
		EventConstructor constructor = new EventConstructor("invoice");
		System.out.println("Event JSOn : "
				+ constructor.getEventPayload( details));
	}
}
