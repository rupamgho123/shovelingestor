
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class SourceConnectionManager {

	Logger logger = Logger.getLogger(SourceConnectionManager.class);
	public static Connection mysqlConnection;
	String hostName;
	String dbName;
	String username;
	String password;

	SourceConnectionManager(String hostName, String dbName, String username,
			String password) {
		this.hostName = hostName;
		this.dbName = dbName;
		this.username = username;
		this.password = password;	
	}

	public void connect() throws ClassNotFoundException, SQLException {
		logger.info("Tryoing to connect to, host : " + hostName + ", DBName: " + dbName + ", User: " + username);
		Class.forName("com.mysql.jdbc.Driver");
		mysqlConnection = DriverManager.getConnection("jdbc:mysql://"
				+ hostName + "/" + dbName, username, password);
	}
}
